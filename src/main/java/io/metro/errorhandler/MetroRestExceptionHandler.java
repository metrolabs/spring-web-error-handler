package io.metro.errorhandler;

import io.metro.errorhandler.models.MetroRestExceptionBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.TypeMismatchException;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

@SuppressWarnings({"unused", "Duplicates"})
@ControllerAdvice
public class MetroRestExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(MetroRestExceptionHandler.class);

    // 400
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            final MethodArgumentNotValidException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final Set<String> errors = new HashSet<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }

        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody body =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors, path);
        return handleExceptionInternal(ex, body, headers, body.getHttpStatus(), request);
    }

    // 400
    @ExceptionHandler({ InvalidDataAccessApiUsageException.class, IllegalArgumentException.class})
    public ResponseEntity<Object> handleInvalidDataAccessApiUsageException(final Exception ex,
                                                                           final WebRequest request)
    {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);

        Set<String> error = new HashSet<>();
        error.add("invalid parameter error occurred");

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody body =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(body, new HttpHeaders(), body.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleBindException(
            final BindException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final Set<String> errors = new HashSet<>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody body =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), errors, path);
        return handleExceptionInternal(ex, body, headers, body.getHttpStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleTypeMismatch(
            final TypeMismatchException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        Set<String> error = new HashSet<>();
        error.add(ex.getValue() + " value for " + ex.getPropertyName() + " should be of type " + ex.getRequiredType());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody body =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(body, new HttpHeaders(), body.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestPart(
            final MissingServletRequestPartException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        Set<String> error = new HashSet<>();
        error.add(ex.getRequestPartName() + " part is missing");

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody body =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(body, new HttpHeaders(), body.getHttpStatus());
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            final MissingServletRequestParameterException ex,
            final HttpHeaders headers, final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        Set<String> error = new HashSet<>();
        error.add(ex.getParameterName() + " parameter is missing");

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            final MethodArgumentTypeMismatchException ex,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        Set<String> error = new HashSet<>();
        error.add(ex.getName() + " should be of type " + ex.getRequiredType().getName());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    @ExceptionHandler({ConstraintViolationException.class})
    public ResponseEntity<Object> handleConstraintViolation(
            final ConstraintViolationException ex,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final Set<String> errors = new HashSet<>();
        for (final ConstraintViolation<?> violation : ex.getConstraintViolations()) {
            errors.add(violation.getRootBeanClass().getName() + " " + violation.getPropertyPath() + ": " + violation.getMessage());
        }

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.CONFLICT, ex.getLocalizedMessage(), errors, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 409
    @ExceptionHandler({DataIntegrityViolationException.class, SQLException.class, DataAccessException.class})
    public ResponseEntity<Object> handleDataIntegrityViolationException(
            final DataIntegrityViolationException ex,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final Set<String> errors = new HashSet<>();
        errors.add(ex.getMessage() + " " + ex.getLocalizedMessage());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.CONFLICT, ex.getLocalizedMessage(), errors, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 404
    @Override
    protected ResponseEntity<Object> handleNoHandlerFoundException(
            final NoHandlerFoundException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        Set<String> error = new HashSet<>();
        error.add("No handler found for " + ex.getHttpMethod() + " " + ex.getRequestURL());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.NOT_FOUND, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 404
    @ExceptionHandler({ NullPointerException.class, FileNotFoundException.class })
    protected ResponseEntity<Object> handleNullPointerException(
            final Exception ex,
            final WebRequest request)
    {
        Set<String> error = new HashSet<>();
        error.add("A NullPointerException occurred");
        String message = "Resource not found";
        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
            message += " for " + path;
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.NOT_FOUND, message, error, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 405
    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
            final HttpRequestMethodNotSupportedException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final StringBuilder builder = new StringBuilder();
        final Set<String> errors = new HashSet<>();
        builder.append(ex.getMethod());
        builder.append(" method is not supported for this request. Supported methods are ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t).append(" "));
        errors.add(builder.toString());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.METHOD_NOT_ALLOWED, ex.getLocalizedMessage(), errors, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 415
    @Override
    protected ResponseEntity<Object> handleHttpMediaTypeNotSupported(
            final HttpMediaTypeNotSupportedException ex,
            final HttpHeaders headers,
            final HttpStatus status,
            final WebRequest request)
    {
        logger.info(ex.getClass().getName());

        final StringBuilder builder = new StringBuilder();
        final Set<String> errors = new HashSet<>();
        builder.append(ex.getContentType());
        builder.append(" media type is not supported. Supported media types are ");
        ex.getSupportedMediaTypes().forEach(t -> builder.append(t).append(" "));
        errors.add(builder.toString());

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.UNSUPPORTED_MEDIA_TYPE,
                        ex.getLocalizedMessage(), errors, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }

    // 500
    @ExceptionHandler({ Exception.class })
    public ResponseEntity<Object> handleAll(final Exception ex, final WebRequest request) {
        logger.info(ex.getClass().getName());
        logger.error("error", ex);

        Set<String> error = new HashSet<>();
        error.add("Unhandled Exception occurred");

        String path = "";
        if (request instanceof ServletWebRequest) {
            ServletWebRequest servletRequest = (ServletWebRequest) request;
            path = servletRequest.getRequest().getRequestURI();
        }

        final MetroRestExceptionBody metroRestExceptionBody =
                new MetroRestExceptionBody(HttpStatus.INTERNAL_SERVER_ERROR, ex.getLocalizedMessage(), error, path);
        return new ResponseEntity<>(metroRestExceptionBody, new HttpHeaders(), metroRestExceptionBody.getHttpStatus());
    }
}
