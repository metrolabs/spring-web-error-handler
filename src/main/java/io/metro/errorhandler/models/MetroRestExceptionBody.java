package io.metro.errorhandler.models;

import org.springframework.http.HttpStatus;

import java.sql.Timestamp;
import java.time.Clock;
import java.time.Instant;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

public class MetroRestExceptionBody {
    private Timestamp timestamp;
    private int status;
    private HttpStatus httpStatus;
    private String message;
    private Set<String> errors;
    private String errorId;
    private String path;

    public MetroRestExceptionBody(HttpStatus httpStatus, String message, Set<String> errors, String path) {
        this.timestamp = new Timestamp(Date.from(Instant.now(Clock.systemUTC())).getTime());
        this.httpStatus = httpStatus;
        this.status = httpStatus.value();
        this.message = message;
        this.errors = errors;
        this.errorId = UUID.randomUUID().toString();
        this.path = path;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public MetroRestExceptionBody setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public int getStatus() {
        return status;
    }

    public MetroRestExceptionBody setStatus(int status) {
        this.status = status;
        return this;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public MetroRestExceptionBody setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public MetroRestExceptionBody setMessage(String message) {
        this.message = message;
        return this;
    }

    public Set<String> getErrors() {
        return errors;
    }

    public MetroRestExceptionBody setErrors(Set<String> errors) {
        this.errors = errors;
        return this;
    }

    public String getErrorId() {
        return errorId;
    }

    public MetroRestExceptionBody setErrorId(String errorId) {
        this.errorId = errorId;
        return this;
    }

    public String getPath() {
        return path;
    }

    public MetroRestExceptionBody setPath(String path) {
        this.path = path;
        return this;
    }
}
